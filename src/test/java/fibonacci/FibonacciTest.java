package fibonacci;

import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Before;

public class FibonacciTest {
	private Fibonacci fibonacci;
	@Before
	public void start(){
		fibonacci=new Fibonacci();
	}
	@Test
	public void Fibonnaci0mustbe0(){
		int expectedValue=0;
		int receivedValue=fibonacci.compute(0,0);
		assertEquals(expectedValue,receivedValue);
	}
	@Test
	public void Fibonnaci1month0pairsmustbe1(){
		int expectedValue=1;
		int receivedValue=fibonacci.compute(1,0);
		assertEquals(expectedValue,receivedValue);
	}
	@Test
	public void Fibonnaci1month1pairsmustbe1(){
		int expectedValue=1;
		int receivedValue=fibonacci.compute(1,1);
		assertEquals(expectedValue,receivedValue);
	}
	@Test
	public void Fibonnaci5months3pairsmustbe3(){
		int expectedValue=19;
		int receivedValue=fibonacci.compute(5,3);
		assertEquals(expectedValue,receivedValue);
	}
	
	@Test(expected=RuntimeException.class)
	public void Montcantbehigherthan40(){
		fibonacci.compute(41,0);
		
	}
	@Test(expected=RuntimeException.class)
	public void Pairscantbehigherthan5(){
		fibonacci.compute(4,10);
		
	}
	@Test(expected=RuntimeException.class)
	public void Pairsmustbeapossitivenumber(){
		fibonacci.compute(4,-1);
		
	}
	@Test(expected=RuntimeException.class)
	public void Monthssmustbeapossitivenumber(){
		fibonacci.compute(-4,0);
		
	}
}
